# Wstępna wersja programu umożliwiającego kalibrację prędkości kół robota
import time
import numpy as np
import cv2
import serial
import serial.tools.list_ports
from tabulate import tabulate
import csv
import matplotlib.pyplot as plt

l_hsv = [0, 0, 0]           # Domyślne wartości zakresów kolorów (używane do bounding boxów)
h_hsv = [255, 255, 255]     #
l_hsv2 = [0, 0, 0]          #
h_hsv2 = [255, 255, 255]    #
wait_time = 1       # Czas oczekiwania na rozpędzenie się silników (w sekundach)
measure_time = 5    # Czas wykonywania pomiarów (w sekundach)
measure_start = 5   # Początkowa wartość zadaniej prędkości podczas pomiaru
measure_step = 5    # Krok zadanej prędkości
measure_end = 255   # Końcowa wartość zadanej prędkości podczas pomiaru


def mouse_event(event, x, y, flags, params):    # Obsługa zdarzeń myszy
    global l_hsv    # TODO: Pozbyć się zmiennych globalnych
    global h_hsv
    global l_hsv2
    global h_hsv2
    if event == cv2.EVENT_LBUTTONDBLCLK or event == cv2.EVENT_RBUTTONDBLCLK:
        h = hsv[y, x, 0]
        s = hsv[y, x, 1]
        v = hsv[y, x, 2]
        h1 = np.mod((h - 20), 256)  # Poszerzenie zakresu hue (modulo 256)
        h2 = np.mod((h + 20), 256)
        if h1 > h2:                 # Oraz zakresów saturation i value (bez modulo)
            buf = h2
            h2 = h1
            h1 = buf
        s1 = s - 50
        if s1 < 0:
            s1 = 0
        s2 = s + 50
        if s2 > 255:
            s2 = 255
        v1 = v - 50
        if v1 < 0:
            v1 = 0
        v2 = v + 50
        if v2 > 255:
            v2 = 255
        if event == cv2.EVENT_LBUTTONDBLCLK:    # Jeżeli dwukrotnie naciśnieto lewy klawisz to przypisz do pierwszego
            l_hsv = [h1, s1, v1]                # bounding boxa
            h_hsv = [h2, s2, v2]
        else:                                   # Jeżeli dwukrotnie naciśnieto prawy klawisz to przypisz do drugiego
            l_hsv2 = [h1, s1, v1]               # bounding boxa
            h_hsv2 = [h2, s2, v2]


def find_sticker(low_hsv, up_hsv, hsv):
    lower_red = np.array(low_hsv)
    upper_red = np.array(up_hsv)

    mask = cv2.inRange(hsv, lower_red, upper_red)           # Operacje na ramce mające na celu pokrycie interesującego
    kernel = np.ones((5, 5), np.uint8)                      # obszaru maską
    kernel2 = np.ones((9, 9), np.uint8)                     #
    erosion = cv2.erode(mask, kernel, iterations=2)         #
    dilation = cv2.dilate(erosion, kernel2, iterations=2)   #
    mask = dilation                                         #

    ret, thresh = cv2.threshold(mask, 40, 255, 0)           # Poszukiwanie konturów na masce
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    pos1 = (0, 0)
    pos2 = (0, 0)
    if len(contours) != 0:              # Jeżeli znaleziono jakikolwiek kontur
        # Obrysowanie wszyskich konturów
        cv2.drawContours(frame, contours, -1, 255, 3)

        # Szukanie największego konturu
        c = max(contours, key=cv2.contourArea)
        x1, y1, w1, h1 = cv2.boundingRect(c)

        # Rysowanie bounding box'ów wokół największego konturu
        cv2.rectangle(frame, (x1, y1), (x1 + w1, y1 + h1), (int(up_hsv[0]), int(up_hsv[1]), int(up_hsv[2])), 2)
        return int(x1+w1/2), int(y1+h1/2)   # Zwraca położenie środka bounding boxa
    return 0, 0                             # Jeżeli nie znaleziono konturu zwraca (0, 0)


ports = []              # Listowanie dostępnych portów szeregowych i nazw urządzeń
while len(ports) == 0:
    ports = list(serial.tools.list_ports.comports())
    if len(ports) == 0:
        input('Nie wykryto żadnego dostępnego portu szeregowego, podłącz robota i naciśnij "Enter".')
print('Dostępne porty szeregowe:')
for p in ports:
    print(p)
if len(ports) != 1:
    s_port = input('Podaj nazwę portu szeregowego do którego podłączony jest robot (w Windows np. COM0, Linux np. '
               '/dev/ttyUSB0): ')
else:
    s_port = str(ports[0].device)
    print('Wybrano jedyne dostępne urządzenie szeregowe ' + s_port)

arduino = serial.Serial(s_port, 115200, timeout=0.01) # Połączenie UART z Arduino
arduino.write('STOP\n'.encode())                     # Wyzerowanie prędkości silników

index = 0   # Listowanie dostępnych kamer i przypisanych do nich numerów
not_end = True
while not_end:
    cap = cv2.VideoCapture(index+cv2.CAP_DSHOW)
    if not cap.read()[0]:
        if index != 0:
            not_end = False
        else:
            input('Nie wykryto żadnej dostępnej kamery, podłącz kamerę i naciśnij "Enter".')
            index -= 1
    else:
        print('Dostępna kamera numer ' + str(index))
    cap.release()
    index += 1
cam_num = 0
if index != 2:
    cam_num = input('Podaj numer kamery (np. 0): ')
else:
    print('Wybrano jedyną dostępną kamerę numer 0')

cap = cv2.VideoCapture(int(cam_num)+cv2.CAP_DSHOW)  # Łączenie z kamerą

#cap.set(15, -8.0)  # "Sztywne" ustawienie ekspozycji, nie działa z więszkością kamer

wheel = input('Podaj które koło będzie kalibrowane (FL, FR, BL, BR - wielkimi literami): ')

print('Kliknij dwukrotnie lewym przyciskiem myszy na jedną naklejkę, a następnie dwukrotnie prawym przyciskiem myszy '
      'na drugą naklejkę, tak aby bounding boxy wyświetlły się poprawnie. Natępnie kliknij "a" aby przejść dalej.')
while True:
    ret, frame = cap.read()                                             # Przechwycenie ramki z kamery
    frame_blur = cv2.GaussianBlur(frame, (9, 9), sigmaX=5, sigmaY=5)    # Rozmycie w celu pozbycia się szumu
    hsv = cv2.cvtColor(frame_blur, cv2.COLOR_RGB2HSV)                   # Konwersja na HSV (w celu ekstrakcji koloru)
    point1 = find_sticker(l_hsv, h_hsv, hsv)                         # Szukanie środka boundinboxa (z naklejką)
    point2 = find_sticker(l_hsv2, h_hsv2, hsv)                       #
    frame = cv2.line(frame, point1, point2, (0, 255, 0), thickness=3)   # Rysowanie linii pomiędzy bounding boxami
    cv2.imshow("Frame", frame)
    cv2.setMouseCallback('Frame', mouse_event)
    if cv2.waitKey(1) & 0xFF == ord('a'):   # Zakończ po nacieśnięciu klawisza "a"
        break

m_speed = []        # Prędkość zadana (w skali od 0 do 255)
real_speed = []     # Prędkość pomierozna (w stopniach na sekundę)
for x in range(measure_start, measure_end + 1, measure_step):
    arduino.write((str(wheel) + str(x) + '\n').encode())           # Zadaje robotowi prędkość kół za pośrednictwem UART
    print('Prędkość zadana ustawiona na ' + str(x))
    total_angle = 0
    angle = 0
    run = 0
    start_measure = time.time()
    while time.time() - start_measure < wait_time:  # Czekaj zadany czas aż silnik rozpędzi się do zadanej prędkości
        ret, frame = cap.read()
        cv2.imshow("Frame", frame)          # Podczas oczekiwania wyświetla aktualny obraz z kamery
        if cv2.waitKey(1) & 0xFF == ord('q'):   # Zakończ po naciśnięciu "q" 
            break
    start_measure = time.time()
    while True:     # TODO: Pozbyć się while True, bo brzydko wygląda
            ret, frame = cap.read()
            if run == 1:    # Pierwszą ramkę pomija, ponieważ przyrost kąta będzie równy 0
                start_measure = time.time()     # Więc liczy czas od ramki drugiej
            end_measure = time.time()
            frame_blur = cv2.GaussianBlur(frame, (9, 9), sigmaX=5, sigmaY=5)    # Operacje takie same jak powyżej
            hsv = cv2.cvtColor(frame_blur, cv2.COLOR_RGB2HSV)                   #
            point1 = find_sticker(l_hsv, h_hsv, hsv)                         #
            point2 = find_sticker(l_hsv2, h_hsv2, hsv)                       #
            frame = cv2.line(frame, point1, point2, (0, 255, 0), thickness=3)   #
            prev_angle = angle
            angle = np.rad2deg(np.arctan2(point1[0]-point2[0], point1[1]-point2[1])) # pomiar kąta funkcją atan2
            if angle < 0:
                angle = 360 + angle
            angle_inc = angle - prev_angle
            if angle_inc < 0:
                angle_inc = angle - prev_angle + 360
            if angle_inc > 300 or angle_inc < 1:
                angle_inc = 0
            if run != 0:
                total_angle += angle_inc
            cv2.imshow("Frame", frame)
            run = run + 1
            if cv2.waitKey(1) & 0xFF == ord('q') or end_measure - start_measure >= measure_time: # pomiar trwa 5 sekund
                real_speed.append(total_angle/(end_measure - start_measure))     #lub do naciśnięcia klawisza "q"
                m_speed.append(float(x))
                print('Zmierzona prędkość = ' + str(real_speed[-1]) + ' stopni na sekundę')
                break
arduino.write('STOP0\n'.encode())     # Wyzerowanie silników
cap.release()               # Uwolnienie kamery
cv2.destroyAllWindows()     # Zniszczenie okien
print('Tabela z pomierzonymi danymi:')
print(tabulate([m_speed, real_speed], tablefmt='orgtbl'))

last_zero_index = next(i for i in reversed(range(len(real_speed))) if real_speed[i] == 0.0) # Podczas dopasowywania
m_speed_cpy = m_speed[last_zero_index:]                     # prostej pomijane są pomiary podczas których koło się nie
real_speed_cpy = real_speed[last_zero_index:]               # kręciło
A = np.vstack([m_speed_cpy, np.ones(len(m_speed_cpy))]).T   # Dodaje kolumnę jedynek do wektora z zadaną prędkością
m, c = np.linalg.lstsq(A, real_speed_cpy, rcond=None)[0]    # Rozwiązanie metodą najmniejszych kwadratów
plt.plot(m_speed, real_speed, 'b', label='Pomierzone dane') # Przygotowywanie wykresów
plt.plot(np.array(m_speed), m * np.array(m_speed) + c, 'r', label='Dopasowana prosta')
plt.legend(frameon=False, loc='lower center', ncol=2);
plt.suptitle('Wyniki pomiarów', fontsize=20)
plt.xlabel('Prędkość zadania (w skali od 0 do 255)', fontsize=16)
plt.ylabel('Prędkość pomierzona [°/s]', fontsize=16)
plt.xlim(0, 255)
plot_path = input('Podaj nazwę pliku graficznego do zapisu wykresu (np. plot.jpg): ')
plt.savefig(plot_path)      # Zapisanie wykresu do obrazu
plt.show()

print('Funkcja liniowa dopasowana metodą najmneijszych kwadratów: ' + str(m) + ' * x + (' + str(c) + ')')
print('Strefa nieczułości (minimalna zadana prędkość) wonosi ' + str((last_zero_index + 1) * measure_step
                                                                     + measure_start))

path = input('Podaj nazwę pliku CSV do zapisu (np. pomiar.csv): ')      # Zapis pomiarów do pliku CSV
with open(path, mode='w', newline='') as file:
    file = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    file.writerow(m_speed)
    file.writerow(real_speed)
    file.writerow([m, c, (last_zero_index + 1) * measure_step + measure_start], real_speed[last_zero_index])
