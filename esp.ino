#include <ESP8266WiFi.h>
#ifndef STASSID
#define STASSID  "SSID" //siec wifi do kterj sie podłaczamy
#define STAPSK  "PASSWORD" //hasło tej sieci
#endif

const char* ssid     = STASSID;
const char* password = STAPSK;

const char* host = "192.168.0.0"; //ip hosta
const uint16_t port = 8090; 

String message = "";
bool send_r = true;
long prev_t;

void setup() {
  Serial.begin(115200);
  WiFi.disconnect(true);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  prev_t = millis();
}

void loop() {
  WiFiClient client;
  if (!client.connect(host, port)){
    delay(100);
    return;
  }

  do
  {
    if (send_r)
    {
      Serial.write('r');
      send_r = false;
    }
    
    if (Serial.available())
    {
      message = Serial.readStringUntil('\n'); 
      send_r = true;
    }
  
    if (client.connected()){
      if (millis() - prev_t > 50){  // 50 jest dla 20 fps
        client.print(message);
        prev_t = millis();
      }    
      
    }
  } while (client.connected());
  
  client.stop();
}
