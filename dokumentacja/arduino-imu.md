1. Przegląd dostępnych IMU

Mamy do dyspozycji dwa IMU:

	imu01c 0J8003 2014 - MinIMU-9 v3 
	imu04a 0J9486 2015 - MinImu-9 v5
	
W stosunku do poprzedniej wersji, MinImu-9 v5 posiada nowe 3-osiowe czujniki MEMS firmy ST - żyroskop i akcelerometr LSM6DS33 oraz magnetometr LIS3MDL. Sensory te posiadają większą dokładność oraz zapewniają wyższą stabilność pomiarów od poprzedników. Tak więc będziemy korzystać z drugiego, nowszego IMU.

Specyfikacja MinImu-9 v5:

	Napięcie zasilania: 2,5 V - 5,5 V
	Pobór prądu: 5 mA
	Wbudowany regulator napięcia
	Wbudowany konwerter napięć dla linii magistrali I2C
	(układ współpracuje zarówno z napięciami 5 V jak i 3,3 V)
	Trzy osie: X, Y, Z
	Interfejs komunikacyjny: I2C (TWI) Fast 400 kHz
	Rozdzielczość:
	Akcelerometr: 16 bitów
	Żyroskop: 16 bitów
	Magnetometr: 16 bitów
	Zakresy pomiarowe (konfigurowalne):
	Akcelerometr: ± 2, ± 4, ± 8,  ± 16 g
	Żyroskop: ± 125, ± 245, ± 500, ± 1000, ± 2000 °/s
	Magnetometr:, ± 4, ± 8, ± 12, ± 16 gauss
	Rozmiar: 20 x 13 x 3 mm
	Masa: 0,7 g (bez złącz)
	
	
2. Podłączenie IMU do Arduino

IMU posiada następujące wyprowadzenia:

	SCL - Linia zegarowa magistrali I2C. Stan wysoki równy jest równy napięciu VIN. Niski GND.
	SDA - Linia danych magistrali I2C. Stan wysoki równy jest równy napięciu VIN. Niski GND.
	GND - Potencjał masy układu.
	VIN - Napięcie zasilania od 2,5 V do 5,5 V.
	VDD - W przypadku, gdy napięcie zasilania jest wyższe niż 3,3V, wyprowadzenie może służyć jako wyjście napięciowe 3,3V o wydajności prądowej do 150mA. Gdy napięcie zasilania mieści się w zakresie 2,5V - 3,3V należy podłączyć je do wyprowadzenia VDD
	SA0 - Wejście umożliwiające zmianę adresacji I2C zgodnie z tabelą poniżej. Domyślnie w stanie wysokim, poprzez rezystor 10 kΩ. Pin współpracuje z napięciem 3,3 V, podłączenie 5 V może trwale uszkodzić układ.

Aby podłączyć IMU do Arduino należy połączyć wyprowadzenia SCL i SDA IMU z SCL i SDA Arduino, GND i SA0 podłączamy do GND Arduino, a VIN łączymy z wyjściem 5V na Arduino. 

**ZABRONIONE JEST PODŁĄCZANIE VDD DO NAPIĘCIA, GDY VIN JEST PODŁĄCZONE ORAZ ZABRONIONE JEST PODŁĄCZANIE DO VDD NAPIĘCIA WYŻSZEGO, NIŻ 3.6V**


3. Transfer danych z IMU do Arduino

IMU posiada akcelerometr, żyroskop oraz magnetometr. W przypadku naszego projektu wykorzystujemy wszystkie trzy czujniki. W repozytorium znajdują się datasheety akcelerometru, żyroskopu oraz magnetometru. Transfer danych do Arduino jest ułatwiony dzięki bibliotekom, które udostępnia producent IMU. https://github.com/pololu/lsm6-arduino - tutaj znajduje się biblioteka obsługująca akcelerometr oraz żyroskop, https://github.com/pololu/lis3mdl-arduino - tutaj znajduje się biblioteka obsługująca magnetometr. Odczyt danych polega na wywołaniu odpowiedniej funkcji odczytu z biblioteki czujnika oraz na przemnożeniu odczytanych wartości przez współczynniki podane w datasheetach.


4. Kalibracja IMU

Przy pierwszym odczycie danych z IMU zauważyliśmy, że odczyty są niezerowe mimo, że czujnik pozostawał w bezruchu. Pierwsza kalibracja wygląda następująco: bierzemy sumę N odczytów, dzielimy sumę na N i od kolejnych pomiarów odejmujemy tę średnią wartość odchylenia. Dodatkowo należy skalibrować magnetometr. W tym celu obracamy IMU oraz mierzymy minimalne i maksymalne wartości odczytów z magnetometrów dla każdej osi z osobna. Dzięki temu możemy wyznaczyć zakresy wartości odczytów z magnetometru i je odpowiednio przeskalować. Następnie wyznaczamy średni początkowy kąt, jaki wskazuje magnetometr, aby wyznaczyć wartość o jaką trzeba przesunąć odczyty z magnetometru, aby wynik pokrywał się z układem systemu wizyjnego.

5. Wyznaczanie yaw z żyroskopu i magnetometru

Z żyroskopu można wyznaczyć zmianę kąta, natomiast z magnetometru można wyznaczyć bezwzględny kąt. Żyroskop podaje prędkość kątową, więc po przemnożeniu odczytanej wartości przez interwał czasowy otrzymujemy zmianę kąta. Z kolei w magnetometrze należy wziąć stosunek odczytanych danych z osi x i y do maksymalnych wartości (wyznaczanych w kalibracji), te wartości należy wprowadzić do funkcji atan2, aby otrzymać bezwzględne ułożenie. Jednak odczyty te są obarczone błędami, dlatego trzeba je przefiltrować i połączyć - więcej w dziale filtracji.

6. Wyznaczanie pozycji z akcelerometru

Z akcelerometru można wyznaczyć prędkość i zmianę położenia robota, jednak te dane również trzeba będzie przefiltrować i wynik połączyć z położeniem wyznaczonym za pomocą GPS (czyli naszej kamery).
	
