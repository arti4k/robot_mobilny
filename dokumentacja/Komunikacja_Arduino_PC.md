Komunikacja między Arduino Mega a PC:

Po naradzie z resztą zespołu doszliśmy do wniosku, że zamiast WIFI shielda użyjemy modułu ESP8266 NodeMcu v3, który będzie wysyłał dane do PC. Zanim zaczęliśmy od komunikacji między Arduino a ESP, podłączając przewody RX do TX każdego z modułów. Z faktu łączenia pracy z zespołem IMU w późniejszym etapie projektu zdecydowaliśmy się na montaż prostego obwodu z czujnikiem temperatury LM35DZ.

Schemat układu:

![Alt Text](https://cdn.discordapp.com/attachments/685199513832652832/688031685316706363/d5724d3ba0f4b46c63676d1df52ca2bc.png)

Stworzyliśmy zatem 3 pliki dla każdej platformy:

  - ArduinoSendData.ino dla Arduino Mega który przesyła dane odbierane na pinie A0 z czujnika temperatury oraz przesyła je komunikacją szeregową do modułu ESP8266
  - WiFiClient.ino dla ESP8266, który odbiera informacje z Arduino, łączy się z siecią oraz wysyła dane do serwera utworzonego na PC
  - Server.py dla PC, pierwotna wersja serwera, na który przesyłane są dane

Ze wględu na ograniczenia portu szeregowego (wysyłanie jednego bajtu), aby wysłać daną typu float konieczne jest konwertowanie jej i wysłanie w mniejszych paczkach. Ta zmiana wymusza konieczność wprowadzenia flagi kontrolnej oraz sprawdzanie jej w programie. Dodatkowym problemem jest konieczność wprowadzenia opóźnienia w celu zabezpieczenia się przed utratą lub zmianą kolejności danych.
  
Stworzyliśmy dwie alternatywne wersje wysyłania i odbierania danych:
  
  - Pierwsza wersja - przesyłanie danych typu float (zmiana na liczbę heksadecymalną i wysłanie charów)
  - Druga wersja - przesyłanie danych typu int (z zakresu od 0 do 255)

Pierwsza wersja jest dokładna, jednak wprowadza problemy opisane powyżej.

Druga wersja jest mniej dokładna ponieważ z IMU odbierane są dane z zakresu od 0 do 360 (stopni), zatem konieczne jest mapowanie przez arduino otrzymywanych z IMU wartości oraz ponowne mapowanie (z 255 do 360) na module ESP.Przez to tracimy na dokładności, lecz wysył danych działa wielokrotnie szybciej niż przy pierwszej metodzie.

Konieczna jest zatem konsultacja z grupą w celu ustalenia jakie parametry wysyłu danych są dla nas priorytetowe.

