Metody filtracji danych z IMU:

Podczas poszukiwania informacji na temat metod filtracji danych z czujnika IMU wyraźnie dominowały dwa rozwiązania: filtr komplementarny oraz filtr Kalmana.

Filtr komplementarny:

Zasada komplementarności polega na wzajemnym uzupełnianiu lub dopełnianiu. Zasadę tę wykorzystuje się w konstrukcji filtrów, które integrują informacje pochodzące z różnych, kanałów i czujników pomiarowych. Celem tej filtracji jest wykorzystanie najistotniejszej informacji, którą transportuje dany sygnał pomiarowy, przy jednoczesnym pominięci części będącej jego zakłóceniem. Filtracja ta umożliwia połącznie pomiarów z różnych czujników i uzyskanie informacji o lepszych własnościach. Czujniki powinny zatem cechować się różną charakterystyką częstotliwościową błędów pomiarowych.

W naszym przypadku do poprawnego określenia orientacji w przestrzeni wykorzystujemy czujnik IMU, który składa się z akcelerometru, magnetometru oraz żyroskopu. Pomiary otrzymywane z akcelerometru oraz magnetometru zakłócone są  sygnałem wysokoczęstotliwościowym (szumy), a te z żyroskopu głównie sygnałem wolnozmiennym (błędy zera). Do wyeliminowania zakłóceń szybkozmiennych należy wykorzystać filtry dolnoprzepustowe (LPF) natomiast do usunięcia wolnozmiennego dryftu zera należy użyć filtru górnoprzepustowego (HPF). 

Filtr komplementarny działa na zasadzie połączenia pożądanych charakterystyk z akcelerometru o niskiej częstotliwości z pożądanymi charakterystykami z żyroskopu o wysokiej częstotliwości. Zasadniczo sprowadza się to do filtrowania poszczególnych charakterystyk, a następnie do ich połączenia w celu uzyskania jednego oszacowania o zdecydowanie lepszych własnościach.
 
Filtr Kalmana:

Filtr Kalmana (KF) jest obserwatorem stanu minimalizującym średniokwadratowy błąd estymacji. Oznacza to, że algorytm na podstawie pomiarów wejścia i wyjścia obiektu szacuje jego wewnętrzny stan, a oszacowanie stanu jest statystycznie optymalne. Filtr ten używany jest przede wszystkim do odszumiania pomiarów oraz odtwarzania zmiennych stanu, które nie są dostępne pomiarowo w sposób bezpośredni.

W naszym projekcie wartości kątów eulera możemy wyliczyć z danych pochodzących akcelerometru i relacji pomiędzy wartościami przyśpieszeń we wszystkich trzech osiach, jak i żyroskopu na podstawie zmiany prędkości tego kąta w czasie. Jak już dobrze wiemy, żyroskopy charakteryzują się zjawiskiem dryftu, a akcelerometry sporym szumem. Jeśli jednak połączymy dane z obu czujników z filtrem Kalmana, uzyskamy w ten sposób znacznie lepsze wyniki.

Podsumowanie:

W przypadku naszego projektu, wydaje nam się, że odpowiednim i wystarczającym narzędziem do poprawy jakości danych pobieranych z czujnika IMU będzie filtr komplementarny. Tworzony przez nas pojazd mobilny będzie poruszał się tylko w jednej płaszczyźnie, dlatego też nie przewidujemy potrzeby implementacji filtru Kalmana, gdyż wydaje się on odpowiedni do bardziej złożonych problemów. Ponadto filtr komplementarny posiada wiele zalet: nie wymaga dużej mocy obliczeniowej, jest stosunkowo prosty w implementacji a przy tym wszystkim powinien zapewniać zadowalające wyniki.  

Implementacja filtru komplementarnego:

Do wyliczenia kąta yaw potrzebny jest przefiltrowany sygnał z magnetometru oraz żyroskopu. Tak jak było wspomniane wcześniej aby pozbyć się wysokoczęstotliwościowych zakłóceń pochodzących z magnetometru potrzebny jest filtr dolnoprzepustowy. Usuwa on wszelkie drgania z otrzymanych danych. Natomiast aby pozbyć się zjawiska dryftu z danych pochodzących z żyroskopu należy użyć filtru górnoprzepustowego. Następnie aby otrzymana wartość kąta była jak najlepsza oraz nieobarczona już zjawiskiem dryftu sumujemy poszczególne kąty pochodzące z magnetometru oraz żyroskopu przypisując im odpowiednie wagi (tzw. współczynnik alfa-α filtru komplementarnego). Sprowadza się to do zsumowania poszczególnych wyników, które są wymnożone odpowiednio przez α oraz 1-α, co nazywamy właśnie filtrem komplementarnym. Współczynnik alfa jest liczbą rzeczywistą należącą do przedziału od 0 do 1 i to od niego zależy, który z sygnałów będzie miał większy wpływ na otrzymany wynik (ten z magnetometru czy ten z żyroskopu). Cała procedura klarownie przedstawiona jest na poniższym schemacie blokowym. 

![](https://cdn.discordapp.com/attachments/685554760975384805/689250802501812248/scheme_filter.JPG)

Wstępna implementacja filtru komplementarnego została umieszczona w programie, który znajduje się w repozytorium. 

Odpowiednie dobranie poszczególnych współczynników zarówno filtru komplementarnego jak i filtrów górno- oraz dolno- przepustowych zostanie przeprowadzone podczas testów na docelowym robocie mobilnym. Wyznaczenie współczynników zależne jest od częstotliwości ruchu oraz czasu przesyłania danych.



