Zakłócenia zewnętrzne

W przypadku zamontowania czujnika na docelowym układzie (robot mobilny) mogą pojawić się dodatkowe zewnętrzne zakłócenia, które będą wpływały na jakość danych pochodzących z czujnika IMU. Przewidywane zewnętrzne zakłócenia wpływające na czujnik, z którymi będziemy musieli się zmierzyć podczas naszego projektu to głównie zakłócenia elektromagnetyczne generowane przez pracujące silniki (EMI – electromagnetic interference) oraz możliwe drgania mechaniczne.

W celu zabezpieczenia czujnika przed wpływem zakłóceń postaramy się aby w konstrukcji robota czujnik znajdował się możliwe jak najdalej od silników, co powinno ograniczyć oddziaływanie pola elektromagnetycznego na IMU. Po przeglądzie metod eliminacji takich zakłóceń dowiedzieliśmy się, że drgania mechaniczne można wyeliminować poprzez kabelki, które amortyzują, ponieważ kiedy płytka IMU jest bezpośrednio w złączu, drgania lepiej się przenoszą na czujniki. Natomiast zakłócenia elektromagnetyczne można zniwelować poprzez wykonanie ekranu kabla. Prosta metoda (i będąca w naszym „zasięgu”) na jego realizacje to użycie folii aluminiowej oraz taśmy izolacyjnej.

To tylko wstępne założenia i przegląd metod eliminacji zakłóceń, które mogą wystąpić. Jednak wszystko okaże się kiedy będziemy w stanie wykonać testy na docelowym układzie, naszym robocie mobilnym. 
