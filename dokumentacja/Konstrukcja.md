1.  Sterowanie zdalne za pomocą pada Sony DualShock 3.

![Alt Text](https://cdn.discordapp.com/attachments/685202315375804423/685229914232061967/PS3-sixaxis-dualshock-controller-buttons.png)

Zdecydowaliśmy się na sterowanie zdalne za pomocą kontrolera Sony DualShock 3 ze względu na wbudowany moduł Bluetooth oraz występowanie dwóch gałek analogowych.

Moduł Bluetooth pozwala na zdalną komunikację z robotem przy wykorzystaniu USB host shield do Arduino UNO oraz USB Bluetooth Dongle. Do obsługi pada posłuży biblioteka USB Host Shield Library 2.0. Alternatywnym rozwiązaniem sterowania bezprzewodowego może być wykorzystanie dodatkowego Arduino UNO które będzie przewodowo komunikowało się kontrolerem Sony DualShock 3 za pomocą USB Host Shield oraz dołączonej biblioteki. Następnie sygnał zawierający informacje o zadanych prędkościach kół będzie bezprzewodowo przesyłany do płytki Arduino znajdującej się na konstrukcji robota za pomocą RF 433MHz Transmitter/Receiver Module.

Gałki analogowe posłużą do kontroli prędkości obrotowych kół robota. Wysunięcie lewej gałki do góry powodowało będzie ruch dwóch lewych kół robota do przodu, a wysunięcie lewej gałki na dół ruch dwóch lewych kół robota do tyłu. Analogiczne dla prawej gałki i prawych kół robota. Prędkość kół będzie proporcjonalna do stopnia wysunięcia gałki. Sygnał z analogowych przycisków L2 i R2 posłuży do obracania robota w miejscu. Po wciśnięciu klawisza R2 lewe koła robota będą kręcić się do przodu, a prawe do tyłu. Po wciśnięciu klawisza L2 lewe koła robota będą kręcić się do tyłu, a prawe do przodu. Prędkość kół będzie proporcjonalna do stopnia wciśnięcia przycisku. Naciśnięcie przycisku kierunkowego do góry powodować będzie ruch do przodu z pełną prędkością, natomiast naciśnięcie przycisku kierunkowego na dół powodować będzie ruch do tyłu z pełną prędkością. Naciśnięcie przycisków kierunkowych w lewo lub w prawo będzie powodować obrót robota o ok. 90° odpowiednio w lewo lub prawo. Pozostałe przyciski posłużą do obsługi przyszłych funkcji robota.

W czasie testowania Adafruit Motor Shield v2.3 zauważyliśmy, że poszczególne koła dla takich samych zadanych wartości funkcji setSpeed() (z biblioteki Adafruit Motor Shield V2 Library) kręcą się z różnymi prędkościami. Wynikać to może z różnych stopni zużycia silników. Różnice w prędkości powodują ruch robota po łuku przy maksymalnej prędkości silników. Może to zostać zniwelowane programowo.

2.  Dokumentacja silników, akumulator


-   silniki: [Źródło](http://www.buehler-motor.pl/index.php?site=products&type=773&details=6411); [Żródło2](http://www.buehler-motor.pl/download/1_61_077_EN.pdf) (numer 714)

	- Specyfikacja

		|     Parametr                        |     Wartość     |
		|:------------------------------------|:---------------:|
		|     Model                           |       714       |
		|     Znamionowy Prąd [ A]            |       0.86      |
		|     Znamionowa prędkość [obr./min.] |       40        |
		|     Znamionowy moment [Ncm]         |       100	      |
		|     Moment maksymalny [Ncm]		      |		    140       |
		|     Rezystancja [Ω] 				        |		    4,8	      |
		|     Ilość stopni przełożenia 		    |			   3    		|
		|     Przełożenie					            |		    73	    	|
		|     Masa silnika [g] 				        |		    250   		|

-   akumulator: [źródło](https://botland.com.pl/pl/akumulatory-zelowe/6878-akumulator-zelowy-12v-32ah-5902270727739.html?utm_source=skapiec&utm_medium=pricewars2&utm_campaign=akumulator-zelowy-12v-32ah)

	-	Specyfikacja

		|     Parametr                                |     Wartość    |
		|:--------------------------------------------|:--------------:|
		|     Napięcie znamionowe  [V]                |      12        |
		|     Pojemność  [Ah]  						            |      3,2       |
		|    Praca cykliczna  [V]      				        |   13,5 - 13,8  |
		|    Stan czuwania [V]			  			          |	     140       |
		|     Prąd inicjacji  [A]					            |  poniżej 0,96  |
		|     Wymiary [mm]			  				            |  132 x 68 x 62 |
		|     Masa [kg] 							                |	     1,25   	 |





3.  Do sterowania:

-   Arduino Uno - [źródło](https://botland.com.pl/pl/arduino-moduly-glowne/1060-arduino-uno-rev3-a000066-8058333490090.html)
	-	Napięcie zasilania: od 7 V do 12 V
	- Model: Arduino Uno
	- Mikrokontroler: ATmega328
		- Maksymalna częstotliwość zegara: 16 MHz
		-	Pamięć SRAM: 2 kB
		- Pamięć Flash: 32 kB (5 kB zarezerwowane dla bootloadera)
		- Pamięć EEPROM: 1 kB
	- Porty I/O: 14
	- Wyjścia PWM: 6
	- Ilość wejść analogowych: 6 (kanały przetwornika A/C o rozdzielczości 10 bitów)
	- Interfejsy szeregowe: UART, SPI, I2C
	- Zewnętrzne przerwania
	- Podłączona dioda LED na pinie 13
	- Gniazdo USB A do programowania
	- Złącze DC 5,5 x 2,1 mm do zasilania
	- W zestawie przezroczyste nóżki samoprzylepne

-   USB Host Shield - [źródło](https://botland.com.pl/pl/arduino-shield-ekspandery-wyprowadzen/4792-arduino-usb-host-shield-sterownik-usb-nakladka-dla-arduino-a000004-8058333490632.html)
	-   Zasilanie pobierane z Arduino
	-   Napięcie pracy wyprowadzeń: 5 V
	-   Pobór prądu: do 500 mA
	-   Sterownik: MAX3421E (dokumentacja)
	-   Płytka korzysta z magistrali SPI, są są to piny:
		-   D10, D11, D12, D13 dla Arduino Uno
		-   D10, D50, D51, i D52 dla Arduino Mega
	-   Wbudowane gniazdo USB typ A
	-   Wyprowadzone piny cyfrowe i analogowe Arduino
	-   Dodatkowe gniazda dla popularnym interfejsów komunikacyjnych, np. I2C/TWI
	-   Możliwość podłączenia kolejnych nakładek typu Shield
	-   Współpracuje z Arduino Uno, Mega i pochodnymi

-   motor shield adafruit v2.3 - [źródło](https://kamami.pl/sterowniki-silnikow-dla-arduino/210910-adafruit-motorstepperservo-shield-dla-arduino-ver-23-adafruit-1438.html)

	-	Kompatybilne z płytkami Arduino: Uno, Due, Leonardo i Mega R3
	-   2 złącza do sterowania serwomechanizmami o napięciu 5 V podłączone do timera Arduino
	-   4 mostki H: TB6612 o wydajności 1,2 A przy pracy ciągłej na każdy kanał z zabezpieczeniem termicznym oraz diodami zabezpieczającymi przed prądem wstecznym
	-   Zasilanie silników DC o napięciu od 4,5 do 13,5 V DC
	-   Sterowanie do 4 silników DC z indywidualnym 8-bitową regulacją prędkości
	-   Sterowanie do 2 silników krokowych (unipolarnych lub bipolarnych) z pojedynczą cewką, podwójną cewką, przeplatanych lub z mikro-krokami
	-   Automatyczne wyłączenie końcówek mocy przy włączeniu układu do zasilania
	-   Duże złącza śrubowe do łatwiejszego podłączenia przewodów (18 - 26 AWG)
	-   Wyprowadzony na wierzch przycisk 'Reset" płytki Arduino
	-   Zabezpieczenie przed odwrotnym podłączeniem zasilania zewnętrznego do zasilania silników
	-   Dostępne wiele bibliotek z przykładami sterowania dla Arduino

-   bezpiecznik samochodowy - [źródło](https://botland.com.pl/pl/bezpieczniki-samochodowe/8805-bezpiecznik-samochodowy-mini-10a-10szt-5900804009511.html)

	-   Bezpiecznik samochodowy w rozmiarze Mini (11 mm) o wartości 10 A.

4.  Do wysyłania danych z IMU:

-   Arduino mega adk - [źródło](https://botland.com.pl/pl/produkty-wycofane/1064-arduino-mega-adk-android-rev3.html)

	-   Napięcie zasilania: 7 V do 12 V
	-   Mikrokontroler: ATmega 2560
		-   Maksymalna częstotliwość zegara: 16 MHz
		-   Pamięć SRAM: 8 kB
		-   Pamięć Flash: 256 kB (8kB zarezerwowane dla bootloadera)
		-   Pamięć EEPROM: 4 kB
	-   Piny I/O: 54
	-   Kanały PWM: 14
	-   Ilość wejść analogowych: 16 (kanały przetwornika A/C)
	-   Interfejsy szeregowe: 4xUART, SPI, I2C
	-   Zewnętrzne przerwania
	-   Podłączona dioda LED do pinu 13
	-   Gniazdo USB A do programowania
	-   Złącze DC 5,5 x 2,1 mm do zasilania

-   Moduł WiFi ESP8266 + NodeMCU v3 - [źródło](https://botland.com.pl/pl/moduly-wifi/8241-modul-wifi-esp8266-nodemcu-v3.html)

	-   Moduł oparty na układzie ESP8266
	-   10 GPIO - każdy może działać jako PWM, I2C lub 1-Wire
	-   Wbudowane złącze microUSB
	-   Konwerter USB-UART CH340
	-   Wbudowana antena PCB
	-   Raster wyprowadzeń: 2,54 mm
	-   Rozstaw złącz: 28 mm
	-   Wymiary modułu: 58 x 30 mm

5.  Graf sterowania robotem

	```mermaid
	graph TB;
	IMU --> wysyl
	zasilanie --> sterownie
	style zasilanie left-position:100,right-position:500
	subgraph Arduino uno
	  subgraph motor shield adafruit v2.3
	    subgraph USB Host Shield
	     sterownie[Sterowanie silnikami robota za pomocą pada, sterownie padem za pomocą bluetooth'a]
	    end
	  end
	end
	zasilanie --> wysyl
	sterownie --> koła[4 x silniki DC 1.61.077.714]
	pad[Pad PS3] -.->sterownie
	wysyl -.-> komp[Komputer - przetworzenie i odczyt danych z IMU oraz kamery, wyświetlanie położenia]
	subgraph Arduino Mega
	  subgraph ESP8266
	    wysyl[Wysłanie danych z IMU na komputer oraz wstępne ich przetworzenie]
	  end
	end
    ```
6. Streszczenie pierwszego spotkania:

Zapoznaliśmy się z elementami konstrukcyjnymi robota, zapisaliśmy ich modele w celu zapoznania się z ich specyfikacjami. Sprawdziliśmy prędkość oraz kierunek obrotu silników. Zamieniliśmy silniki tak, aby wszystkie obracały się w jednym kierunku dla ułatwienia pisania programu sterującego silnikami.