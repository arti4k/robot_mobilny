from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
import serial
import serial.tools.list_ports
import csv
import numpy as np
import time


def find_line(x1, y1, x2, y2):  # Funkcja wyznaczająca parametry prostej na podstawie dwóch punktów leżących na niej
    points = [(x1, y1), (x2, y2)]
    x_coords, y_coords = zip(*points)
    a = np.vstack([x_coords, np.ones(len(x_coords))]).T
    return np.linalg.lstsq(a, y_coords, rcond=None)[0]


class MainWindow:       # Głowne okno aplikacji
    def __init__(self, master):
        self.master = master
        self.frame = Frame(self.master)
        self.ports = ['elo']
        self.path_pomiarBL = None
        self.path_pomiarBR = None
        self.path_pomiarFL = None
        self.path_pomiarFR = None

        self.label_server = Label(master, text="Dostępne porty szeregowe:").pack()

        self.list_box = Listbox(self.master, width=50)
        self.load_ports()
        self.list_box.pack()

        self.button_send = Button(master, text="Odśwież porty", width=20, pady=2, command=self.load_ports)
        self.button_send.pack()

        self.label_server = Label(master, text="Plik CSV z pomiarem koła:").pack()

        self.button_send = Button(master, text="tył lewe", width=20, pady=2, command=self.open_csv_bl)
        self.button_send.pack()

        self.button_send = Button(master, text="tył prawe", width=20, pady=2, command=self.open_csv_br)
        self.button_send.pack()

        self.button_send = Button(master, text="przód lewe", width=20, pady=2, command=self.open_csv_fl)
        self.button_send.pack()

        self.button_send = Button(master, text="przód prawe", width=20, pady=2, command=self.open_csv_fr)
        self.button_send.pack()

        self.label_server = Label(master, text="").pack()

        self.button_send = Button(master, text="Aktualizuj dane", width=20, pady=2, command=self.update)
        self.button_send.pack()

    def load_ports(self):
        self.ports = []
        self.ports = list(serial.tools.list_ports.comports())
        self.list_box.delete(0, END)
        for p in self.ports:
            self.list_box.insert(END, p)

    def open_csv_bl(self):
        self.path_pomiarBL = filedialog.askopenfilename(initialdir="", title="Podaj nazwę pliku CSV z pomiarem tylnego "
                                                                             "lewego koła (np. pomiarBL.csv)")

    def open_csv_br(self):
        self.path_pomiarBR = filedialog.askopenfilename(initialdir="", title="Podaj nazwę pliku CSV z pomiarem tylnego "
                                                                             "prawego koła (np. pomiarBR.csv)")

    def open_csv_fl(self):
        self.path_pomiarFL = filedialog.askopenfilename(initialdir="", title="Podaj nazwę pliku CSV z pomiarem przednie"
                                                                             "go lewego koła (np. pomiarFL.csv)")

    def open_csv_fr(self):
        self.path_pomiarFR = filedialog.askopenfilename(initialdir="", title="Podaj nazwę pliku CSV z pomiarem przednie"
                                                                             "go prawego koła (np. pomiarFR.csv)")

    def update(self):
        if len(self.list_box.curselection()) > 0:
            try:
                s_port = str(self.ports[self.list_box.curselection()[0]].device)
                arduino = serial.Serial(s_port, 115200, timeout=0.1, write_timeout=0.1)  # Połączenie UART z Arduino
                arduino.write('STOP0\n'.encode())  # Wyzerowanie prędkości silników
            except:
                messagebox.showerror("Błąd połączenia", 'Nie udało się nawiązać połązcenia z robotem')
                self.load_ports()
                return -1
            paths = [self.path_pomiarBL, self.path_pomiarBR, self.path_pomiarFL, self.path_pomiarFR]
            real_speed = []  # Listy zawierające dane odczytane z pliku CSV, albo obliczone na ich podstawie
            max_speed = []
            min_speed = []
            min_speed_ind = []
            ms = []
            cs = []
            new_max_speed = []
            new_min_speed = []
            number_of_paths = 0
            for path in paths:  # Odczyt danych ze wskazanych prędzej plików CSV
                if path is not None:
                    try:
                        with open(path, newline='') as file:
                            reader = list(csv.reader(file))
                            try:
                                real_speed.append(reader[1])  # Zmierzona prędkość
                                ms.append(float(reader[2][0]))  # Parametry 'm' dopsowanej prostej
                                cs.append(float(reader[2][1]))  # Parametry 'c' dopasowanej prostej
                                min_speed_ind.append(float(reader[2][2]))  # Indeks minimalnej prędkości różnej od zera
                                min_speed.append(float(reader[2][3]))  # Wartość minimalnej prędkości różnej od zera
                                number_of_paths += 1
                            except csv.Error as e:
                                messagebox.showerror("Błąd odczytywania pliku", 'Plik {}: {}'.format(path, e))
                                return -1
                    except:
                        messagebox.showerror("Błąd", 'Błąd odczytywania pliku')
                        return -1

            if number_of_paths == 0:
                messagebox.showerror("Błąd", 'Nie wczytano żadnego pliku z pomiarami')
                return -1
            max_speed = [row[-1] for row in real_speed]  # Odczyt ostatnich elementów list ze zmierzoną prędkością
            # Wyznaczanie wspólnego zakresu prędkości dla wszystkich czterech kół:
            s_min_speed = float(max(min_speed))  # Maksimum z minimalnych prędkości kół
            s_max_speed = float(min(max_speed))  # Minimum z maksmalnych prędkości kół
            for c in cs:  # Obliczenie na podstawie dopasowanej prostej nowych zakresów zadanych prędkości (od 0 do 255)
                new_min_speed.append(s_min_speed - float(c))
                new_max_speed.append(s_max_speed - float(c))
            new_min_speed = [int(x / y) for x, y in zip(new_min_speed, ms)]
            new_max_speed = [int(x / y) for x, y in zip(new_max_speed, ms)]
            params = []  # Lista zawierająca parametry równania prostej "rzutującej" stare zadane na nowy zakres
            for n_min_speed, n_max_speed in zip(new_min_speed, new_max_speed):
                params.append(find_line(0, n_min_speed, 255, n_max_speed))

            # Wysyłanie parametrów do robota w celu zapisania ich w pamięci EEPROM
            wheels_labels = ['BL', 'BR', 'FL', 'FR']
            wheels = []
            for path in zip(paths, range(0, len(paths))):
                if path[0] is not None:
                    wheels.append(wheels_labels[path[1]])
            for wheel in wheels:
                for param in params:
                    arduino.write(('M' + str(wheel) + str(param[0]) + '\n').encode())
                    time.sleep(0.1)
                    arduino.write(('C' + str(wheel) + str(param[1]) + '\n').encode())
                    time.sleep(0.1)
            messagebox.showinfo("Powodzenie", "Zapis ukończony powodzeniem.")
        else:
            messagebox.showerror("Błąd", "Najpierw wybierz urządzenie.")
            return -1


def main():                     # Główna pętla
    root = Tk()
    root.title("Aktualzacja parametrów")
    app = MainWindow(root)
    root.mainloop()


if __name__ == '__main__':      # Punkt wejścia
    main()
