/* Oprogramowanie sterujące robotem */
// Biblioteki odpowiedzialne za komunikację z padem oraz Adafruit Motor
#include <PS3BT.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <EEPROM.h>

#ifdef dobogusinclude
#include <spi4teensy3.h>
#endif
#include <SPI.h>

// Biblioteki do obsługi IMU
#include <LSM6.h>
#include <LIS3MDL.h>
#include <math.h>

#define honk_pin 9  // Dyrektywa przypisująca honk_pin do pinu klaksona
#define led1_pin 7
#define led2_pin 6
#define button_pin 5

#define EEPROMmBL 0               // Przypisanie nazw mnemonicznych 
#define EEPROMcBL sizeof(float)   // Do zakresów pamięci EEPROM
#define EEPROMmBR sizeof(float)*2
#define EEPROMcBR sizeof(float)*3
#define EEPROMmFL sizeof(float)*4
#define EEPROMcFL sizeof(float)*5
#define EEPROMmFR sizeof(float)*6
#define EEPROMcFR sizeof(float)*7
#define CALIBRATION true          // Jeżeli true to wykonywana jest kalibracja

USB Usb;          // "Konstruktor" shielda USB
BTD Btd(&Usb);    // Tworzy instancję Bluetooth Dongle
PS3BT PS3(&Btd);  // Tworzy instancję z biblioteki do obsługi pada poprzez Bluetooth

// "Konstruktor" sielda Adafruit Motor z domyślnym adresem I2C
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
// "Konstruktory" silników DC
Adafruit_DCMotor *MotorFL = AFMS.getMotor(4); // Przedni lewy silnik
Adafruit_DCMotor *MotorFR = AFMS.getMotor(3); // Przedni prawy silnik
Adafruit_DCMotor *MotorBL = AFMS.getMotor(1); // Tylny lewy silnik
Adafruit_DCMotor *MotorBR = AFMS.getMotor(2); // Tylny prawy silnik

// zmienne używane do sterowania robotem
String inString = "";         // String na zmienną wejściową
String inStringText = "";     // String na zmienną wejściową 
String command = "";          // String na komendę otrzymaną z PC
int m_speed = 0;              // Zadana przez PC prędkość kół
bool is_command = false;      // Zmienna informująca o zadanej komendzie
bool led1_on = false;         // Zmienna informująca o włączeniu diody 1
float mBL = 1;                // Zmienne zawierające parametry prostej
float cBL = 0;                // służącej do "rzutowania" zadanych prędkości
float mBR = 1;                // kół robota w zakresie od 0 do 255
float cBR = 0;                // na zakres wynikający z kalibracji kół
float mFL = 1;                //
float cFL = 0;                //
float mFR = 1;                //
float cFR = 0;                //

// zmienne wykorzystywane w obsłudze IMU
LSM6 imu;                       // zawiera żyroskop
LIS3MDL mag;                    // zawiera magnetometr
String data = "NULL";           // dane wysyłane na komputer
double gz_dev = 0, ax_dev = 0, ay_dev = 0; // początkowe średnie odchylenie pomiarów
double gz, yaw_g, g_raw;        // dane z żyroskopu
double mx, my, yaw_m, yaw_m_prev; // dane z magnetometru
unsigned long t_prev;           // poprzedni czas pomiaru
double dt;                      // czas pomiędzy pomiarami
double yaw;                     // końcowy kąt po filtracji
double alpha_lpf = 0.95;         // współczynnik filtru dolnoprzepustowego
double alpha_lpf_acc = 0.995;    // współczynnik filtru dolnoprzepustowego akcelerometru
double alpha_yaw = 0.9;         // współczynnik filtru komplementarnego
double mx_prev, my_prev;        // poprzednie pomiary z magnetometru
double ax_prev, ay_prev;        // poprzednie pomiary z akcelerometru
bool pressed = false;           // zmienna mówiąca o przytrzymaniu klawisza up lub down
double ax = 0, ay = 0, v = 0, a = 0; 
const double g = 9.80665;       
double a_dev = 0;
// wektory zawierające zakresy odpowiednich osi magnetometru
LIS3MDL::vector<int16_t> mag_min = {32767, 32767}, mag_max = {-32768, -32768};


void setup() {
  pinMode(honk_pin, OUTPUT);    // Konfiguracja pinu honk_pin jako wyjście
  pinMode(led1_pin, OUTPUT);    // Konfiguracja pinu honk_pin jako wyjście
  pinMode(led2_pin, OUTPUT);    // Konfiguracja pinu honk_pin jako wyjście
  digitalWrite(honk_pin, LOW);  // Ustawienie stanu niskiego honk_pin (wyłączenie sygnłu dźwiękowego)
  digitalWrite(led1_pin, LOW);  // Ustawienie stanu niskiego led1_pin (zgaszenie led1)
  digitalWrite(led2_pin, LOW);  // Ustawienie stanu niskiego led2_pin (zgaszenie led2)
  
  Serial.begin(115200); // Połączenie UART, potrzebne do debugowania
  Serial1.begin(115200); // Połączenie UART z ESP8266
#if !defined(__MIPSEL__)
  //while (!Serial); // Czekaj na połączenie szeregowe - używane na arduino Leonardo
#endif
  if (Usb.Init() == -1) {
    //Serial.println(F("\r\nNieudane połączenie USB"));
    while (1); // 
  }
  //Serial.println(F("\r\nUdane połączenie USB"));
  
  AFMS.begin();  // Tworzy z domyśną częstotliwością 1.6KHz
  
  // Ustawienie prędkości początkowej i kierunku obrotu silników
  MotorFL->setSpeed(0);
  MotorFR->setSpeed(0);
  MotorBL->setSpeed(0);
  MotorBR->setSpeed(0);
  MotorFL->run(FORWARD);
  MotorFR->run(FORWARD);
  MotorBL->run(FORWARD);
  MotorBR->run(FORWARD);
  // Wyłączenie silników
  MotorFL->run(RELEASE);
  MotorFR->run(RELEASE);
  MotorBL->run(RELEASE);
  MotorBR->run(RELEASE);

  // Odczyt parametrów kalibracyjnych zapisanych w pamięci EEPROM
  EEPROM.get(EEPROMmBL, mBL);
  EEPROM.get(EEPROMcBL, cBL);
  EEPROM.get(EEPROMmBR, mBR);
  EEPROM.get(EEPROMcBR, cBR);
  EEPROM.get(EEPROMmFL, mFL);
  EEPROM.get(EEPROMcFL, cFL);
  EEPROM.get(EEPROMmFR, mFR);
  EEPROM.get(EEPROMcFR, cFR);

  // Inicjalizacja żyroskopu i magnetometru
    Wire.begin();
    
    if (!imu.init()){
        //Serial.println("Failed to detect and initialize IMU!");
        while(1);
    }
    if (!mag.init())
    {
        //Serial.println("Failed to detect and initialize magnetometer!");
        while (1);
    }
    //Serial.println("Działa!");
    
    imu.enableDefault(); // ustawia akcelerometr na pracę +-2g a żyroskop na +-245dps
    mag.enableDefault(); // ustawia magnetometr na pracę +-4gauss
    
    delay(100);
    t_prev = micros();
    if (CALIBRATION) calibration(true);
}
void loop() {
  Usb.Task(); // Obsługa połączenia USB z padem
  // Zmienna opisująca czy silniki robota powinny się poruszać
  bool is_ride = false;
  // Zmienne opisujące stan przycików w kontrolerze
  bool b_left = false;
  bool b_right = false;
  bool b_down = false;
  bool b_up = false;
  bool b_honk = false;
  int left = 0;
  int right = 0;
  int l_motor_speed = 0;
  int r_motor_speed = 0;
  int l_analog_y = 0;
  int l_analog_x = 0;
  int r_analog_y = 0;
  int r_analog_x = 0;

  float f_number = uart_pc();   // Odczyt danych z UART
  
  if (is_command) {             // Dekodowanie komend wysłanych z PC
    decode_command(f_number);
  }
  
  read_pad(&b_left, &b_right, &b_down, &b_up, &b_honk, &left, &right, &l_analog_y, &l_analog_x, &r_analog_y, &r_analog_x);    // Odczyt danych z pada
  int diff = right - left;  // Różnica w stopniu naciśnięcia przycisków L2 i R2, mówi o prędkości robota do tyłu lub przodu
  
  if (diff > 0){  // Ruch robota do przodu
    is_ride = true;
    l_motor_speed = r_motor_speed = diff;
    if (l_analog_x < -24){
      l_motor_speed = l_motor_speed * ((255.0 - 2.0 * (float)abs(l_analog_x)) / 255.0);
    } else if (l_analog_x > 24) {
      r_motor_speed = r_motor_speed * ((255.0 - 2.0 * (float)abs(l_analog_x)) / 255.0);
    }
  } else if (diff < 0){   // Ruch robota do tyłu
    is_ride = true;
    l_motor_speed = r_motor_speed = diff;
    if (l_analog_x < -24){
      l_motor_speed = l_motor_speed * ((255.0 - 2.0 * (float)abs(l_analog_x)) / 255.0);
    } else if (l_analog_x > 24) {
      r_motor_speed = r_motor_speed * ((255.0 - 2.0 * (float)abs(l_analog_x)) / 255.0);
    }
  }

  if (b_honk){  // Aktywacja klaskona za pomocą przycisku "X"
    tone(honk_pin, 1000);
  } else {
    noTone(honk_pin);
  }
  
  run_motors(l_motor_speed, r_motor_speed, is_ride);   // Zadanie prędkości kołom

  // Część pętli odpowiadająca za IMU oraz wysyłanie danych na komputer
  dt = static_cast<double>(micros() - t_prev) * 0.000001;
  t_prev = micros();

  // odczyt danych z imu
  imu_read();
  scaled_mag();

  // yaw z magnetometru
  yaw_m = atan2(mx, my) * 180 / M_PI;
  double delta_m = yaw_m - yaw_m_prev;
  if(delta_m > 180){
    yaw_m = yaw_m_prev + delta_m - 360;
  }
  else if(delta_m < -180){
    yaw_m = yaw_m_prev + delta_m + 360;
  }

  // przyrost kąta z żyroskopu
  g_raw = gz * dt;

  // yaw z żyroskopu
  yaw_g = yaw + g_raw;
  
  if(yaw_g > 360 && yaw_m > 360){
    yaw_g -= 360;
    yaw_m -= 360;
  }
  else if(yaw_g < 0 && yaw_m < 0){
    yaw_g += 360;
    yaw_m += 360;
  }

  yaw_m_prev = yaw_m;


  // filtr komplementarny dla yaw
  yaw = alpha_yaw * (yaw_g) + (1 - alpha_yaw) * yaw_m;

  // wyliczenie prędkości i przyspieszenia

  a = ax - a_dev;

  v += dt * a;

  if (diff == 0){
    v = 0;
    a_dev += a;
    a = 0;
  }
    
  // wysyłanie danych na komputer przy pojawieniu się odpowiedniej flagi
  if(Serial1.read() == 'r'){
    double yaw_to_send = fmod(yaw + 360, 360);    // aktualizacja danych do wysłania
    int voltage = analogRead(A0);                 // odczyt przetwornika ADC
    data = String(yaw_to_send) + " " + String(v) + " " + String(a) + " " + String(voltage);
    Serial1.println(data);
    led1_on = !led1_on;   // Miganie diodą 1
    if (led1_on){
      digitalWrite(led1_pin, HIGH);
    } else {
      digitalWrite(led1_pin, LOW);
    }
  }
}

float uart_pc() {
  // Zmienna zawierająca liczbę zmiennoprzecinkową odczytaną z UART
  float f_number = 0;

  while (Serial.available() > 0) {  // Komunikacja szeregowa z PC
    int inChar = Serial.read();
    if (isDigit(inChar) || inChar == '.') { // Odczyt liczb
      inString += (char)inChar;
    } else if (inChar != '\n'){ // Odczyt komend
      inStringText += (char)inChar;
    }
    if (inChar == '\n') {
      m_speed = inString.toInt();
      f_number = inString.toFloat();
      command = inStringText;
      inString = "";
      inStringText = "";
      is_command = true;
      digitalWrite(led2_pin, LOW);
    }
  }
  return(f_number);
}

void decode_command(float f_number) {
  if (command == "FL" || command == "ALL") {    // Nadanie prędkości kół przez program kalibrujący
      MotorFL->run(BACKWARD);
      MotorFL->setSpeed(m_speed);
    }
    if (command == "BL" || command == "ALL"){
      MotorBL->run(BACKWARD);
      MotorBL->setSpeed(m_speed);
    }
    if (command == "FR" || command == "ALL"){
      MotorFR->run(FORWARD);
      MotorFR->setSpeed(m_speed);
    }
    if (command == "BR" || command == "ALL") {
      MotorBR->run(FORWARD);  
      MotorBR->setSpeed(m_speed);
    }
    if (command == "STOP") {    // Zaptrzymanie wszystkich kół i przerwanie kalibracji
      MotorFL->setSpeed(0); 
      MotorBL->setSpeed(0); 
      MotorFR->setSpeed(0);
      MotorBR->setSpeed(0);
      is_command = false;
      digitalWrite(led2_pin, HIGH);
    }
    if (command == "MBL") {     // Zapisanie do pamięci EEPROM odpowiedniej danej kalibracyjnej wysłanej z PC
      EEPROM.put(EEPROMmBL, f_number);
      mBL = f_number;
      is_command = false;
      digitalWrite(led2_pin, HIGH);
    }
    if (command == "CBL") {
      EEPROM.put(EEPROMcBL, f_number);
      cBL = f_number;
      is_command = false;
      digitalWrite(led2_pin, HIGH);
    }
    if (command == "MBR") {
      EEPROM.put(EEPROMmBR, f_number);
      mBR = f_number;
      is_command = false;
      digitalWrite(led2_pin, HIGH);
    }
    if (command == "CBR") {
      EEPROM.put(EEPROMcBR, f_number);
      cBR = f_number;
      is_command = false;
      digitalWrite(led2_pin, HIGH);
    }
    if (command == "MFL") {
      EEPROM.put(EEPROMmFL, f_number);
      mFL = f_number;
      is_command = false;
      digitalWrite(led2_pin, HIGH);
    }
    if (command == "CFL") {
      EEPROM.put(EEPROMcFL, f_number);
      cFL = f_number;
      is_command = false;
      digitalWrite(led2_pin, HIGH);
    }
    if (command == "MFR") {
      EEPROM.put(EEPROMmFR, f_number);
      mFR = f_number;
      is_command = false;
      digitalWrite(led2_pin, HIGH);
    }
    if (command == "CFR") {
      EEPROM.put(EEPROMcFR, f_number);
      cFR = f_number;
      is_command = false;
      digitalWrite(led2_pin, HIGH);
    }
}

void read_pad(bool *b_left, bool *b_right, bool *b_down, bool *b_up, bool *b_honk, int *left, int *right, int *l_analog_y, int *l_analog_x, int *r_analog_y, int *r_analog_x) {
  if (PS3.PS3Connected & !is_command) {   // Funkcja odczytująca dane z pada DualShock 3 
    *left = PS3.getAnalogButton(L2); // Stopień wciśnięcia przycisku L2, wartości od 0 do 255
    *right = PS3.getAnalogButton(R2); // Stopień wciśnięcia przycisku L2, wartości od 0 do 255
    *l_analog_y = (PS3.getAnalogHat(LeftHatY)-128)*2+1; // Wychylenie lewej gałki analogowej w pionie, wartości od -255 do 255
    *r_analog_y = (PS3.getAnalogHat(RightHatY)-128)*2+1; // Wychylenie prawej gałki analogowej w pionie, wartości od -255 do 255
    *l_analog_x = (PS3.getAnalogHat(LeftHatX)-128)*2+1; // Wychylenie lewej gałki analogowej w poziomie, wartości od -255 do 255
    *r_analog_x = (PS3.getAnalogHat(RightHatX)-128)*2+1; // Wychylenie prawej gałki analogowej w ppoziomie, wartości od -255 do 255
    *b_left = PS3.getButtonClick(LEFT);    // Informacja o naciśnięciu klawiszy kierunkowych
    *b_right = PS3.getButtonClick(RIGHT);  //
    *b_down = PS3.getButtonPress(DOWN);    //
    *b_up = PS3.getButtonPress(UP);        //
    *b_honk = PS3.getButtonPress(CROSS);   // Informacja o naciśnięciu klawisza "X" - trąbienie
  }
}

void run_motors(int l_motor_speed, int r_motor_speed, bool is_ride) {
  if (is_ride & !is_command){   // Zadanie prędkości silnikom gdy nie jest wykonywana komenda wysłana z PC 
    if (l_motor_speed >= 0){
      MotorFL->run(BACKWARD);
      MotorBL->run(BACKWARD);
      MotorFL->setSpeed(int(l_motor_speed*mFL+cFL));
      MotorBL->setSpeed(int(l_motor_speed*mBL+cBL));
    } else {
      MotorFL->run(FORWARD);
      MotorBL->run(FORWARD);
      MotorFL->setSpeed(int(-l_motor_speed*mFL+cFL));
      MotorBL->setSpeed(int(-l_motor_speed*mBL+cBL));
    }
    if (r_motor_speed >= 0){
      MotorFR->run(BACKWARD);
      MotorBR->run(BACKWARD);
      MotorFR->setSpeed(int(r_motor_speed*mFR+cFR));
      MotorBR->setSpeed(int(r_motor_speed*mBR+cBR));
    } else {
      MotorFR->run(FORWARD);
      MotorBR->run(FORWARD);
      MotorFR->setSpeed(int(-r_motor_speed*mFR+cFR));
      MotorBR->setSpeed(int(-r_motor_speed*mBR+cBR));
    }
  } else if (!is_command) { // Dezaktywacja silników
    MotorFL->setSpeed(0);
    MotorFR->setSpeed(0);
    MotorBL->setSpeed(0);
    MotorBR->setSpeed(0);
    MotorFL->run(FORWARD);
    MotorFR->run(FORWARD);
    MotorBL->run(FORWARD);
    MotorBR->run(FORWARD);
    MotorFL->run(RELEASE);
    MotorFR->run(RELEASE);
    MotorBL->run(RELEASE);
    MotorBR->run(RELEASE);
  }
}

// Funkcje do obsługi IMU

void imu_calibration(int probes){ // wstępna kalibracja IMU - sumuje N pomiarów, sumę dzieli na N wyznaczając średnie odchylenie
  double gz_sum, ax_sum, ay_sum;
  gz_sum = 0;
  ay_sum = 0;
  ax_sum = 0;
  for(int i = 0; i < probes; i++){
    imu.read();
    gz_sum += static_cast<double>(imu.g.z);
    ax_sum += static_cast<double>(imu.a.x);
    ay_sum += static_cast<double>(imu.a.y);
  }
  gz_dev = gz_sum / probes;
  ax_dev = ax_sum / probes;
  ay_dev = ay_sum / probes;
}

void mag_calibration(int turn_num, int probes){
  mag_min = {32767, 32767}, mag_max = {-32768, -32768};
  mag.read();
  mx_prev = mag.m.x;
  my_prev = mag.m.y;

  t_prev = micros();
  double rotation = 0;

  // robot zaczyna się obracać, żeby wyznaczyć zakresy magnetometru
  
  // tutaj powinien być kod do obrotu w prawo z małą prędkością
  run_motors(-80, 80, true);
  
  while(abs(rotation) < turn_num * 360){
    mag_read();
    // korekta zakresu wartości magnetometru
    mag_min.x = min(mag_min.x, mag.m.x);
    mag_min.y = min(mag_min.y, mag.m.y);
    mag_max.x = max(mag_max.x, mag.m.x);
    mag_max.y = max(mag_max.y, mag.m.y);
    // to czy robot wykonał wyznaczony obrót będzie sprawdzane za pomocą kąta obliczonego z żyroskopu
    imu_read();
    double dt = static_cast<double>(micros() - t_prev) * 0.000001;
    t_prev = micros();
    rotation += gz * dt;
  }
  run_motors(0, 0, false);
  // tutaj powinien być kod zatrzymujący robota
  
  delay(100);
  // wyznaczanie startowego kąta z magnetometru tu robot powinien stać w miejscu
  double yaw_m_avg = 0;
  for(int i = 0; i < probes; i++){
    scaled_mag();
    yaw_m = atan2(mx, my) * 180 / M_PI;
    if(yaw_m < 0) yaw_m += 360;
    yaw_m_avg += yaw_m / probes;
  }
  yaw_m = yaw_m_avg;
}

void calibration(bool calibrate_imu){
  digitalWrite(led2_pin, LOW);
    if (calibrate_imu)
    {
      imu_calibration(1000);    // wstępna kalibracja żyroskopu
      mag_calibration(1, 1000);       // wstępna kalibracja magnetometru
    }
    
    yaw = yaw_m;
    yaw_m_prev = yaw_m;

    while(!Serial1.available());
    if(Serial1.read() == 'r') 
        Serial1.println("first");
    delay(1000);
    
    run_motors(100, 100, true);   // Jazda kawałek do przodu
    delay(1000);
    run_motors(0, 0, true);
    
    while(!Serial1.available());
    if(Serial1.read() == 'r') 
        Serial1.println("second");
    
    delay(100);
    t_prev = micros();
    digitalWrite(led2_pin, HIGH);
}

void imu_read(){
  // odczyt danych z akcelerometru i żyroskopu
  imu.read();         

  // korekta odczytów o średnie odchylenie
  gz = static_cast<double>(imu.g.z) - gz_dev;
  ax = alpha_lpf_acc * ax_prev + (1 - alpha_lpf_acc) * (static_cast<double>(imu.a.x) - ax_dev);
  ay = alpha_lpf_acc * ay_prev + (1 - alpha_lpf_acc) * (static_cast<double>(imu.a.y) - ay_dev);
  
  ax_prev = ax;
  ay_prev = ay;

  // skalowanie odczytów o wartości podane w datasheecie
  gz *= 8.75 * 0.001;
  ax *= 0.061 * g * 0.001;
  ay *= 0.061 * g * 0.001;
}

void mag_read(){
  // odczyt danych z magnetometru
  mag.read();

  // lpf dla danych z magnetometru
  mx = alpha_lpf * mx_prev + (1 - alpha_lpf) * static_cast<double>(mag.m.x);
  my = alpha_lpf * my_prev + (1 - alpha_lpf) * static_cast<double>(mag.m.y);
  mx_prev = mx;
  my_prev = my;

  }
  
void scaled_mag(){
  mag_read();
  // skalowanie danych z magnetometru na wartości <-1, 1>
  mx = static_cast<double>(2 * mx - mag_max.x - mag_min.x) / (mag_max.x - mag_min.x);
  my = static_cast<double>(2 * my - mag_max.y - mag_min.y) / (mag_max.y - mag_min.y);

}